﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SD_Tema4.Models
{
    public class Activities
    {
        public int Id { get; set; }
        public string Activity { get; set; }
    }
}
