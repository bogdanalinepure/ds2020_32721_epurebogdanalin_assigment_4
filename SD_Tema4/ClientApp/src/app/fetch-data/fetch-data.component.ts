import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { FormBuilder } from '@angular/forms';

interface Patient {
    id: number
    name: string,
    diagnostic: string
}

interface Single {
    name: string,
    value: number
}

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})

export class FetchDataComponent {
    public patients: Patient[];
    public patient: any;
    public activities: any[];
    private url: string;
    public isPatientSelected = false;

    single: Single[] = [];
    view: any[] = [500, 400];
    legend: boolean = true;
    legendPosition: string = 'below';

    public recomandations: string[] = [];

    checkoutForm;

    colorScheme = {
        domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
    };

    constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string, private formBuilder: FormBuilder) {
      this.url = baseUrl;
      http.get<Patient[]>(baseUrl + 'api/patients').subscribe(result => {
          this.patients = result;
          console.warn(this.patients);
      }, error => console.error(error));

        this.checkoutForm = this.formBuilder.group({
            recomandation: '',
        });
    }

    getPatientData(patientId: number) {
        this.reset();
        console.log(patientId);
        this.http.get<any>(this.url + 'api/patients/activities/' + patientId).subscribe(res => {
            this.activities = res;
            this.activities.forEach(
                x => {
                    var com = { name: x.activity, value: 100/this.activities.length };
                    this.single.push(com);
                }
            )
            this.isPatientSelected = true;
            this.patient = this.patients.filter(pat => pat.id == patientId)[0];
            console.log(this.single)
        });
    };

    onSelect(data): void {
        console.log('Item clicked', JSON.parse(JSON.stringify(data)));
    }

    onActivate(data): void {
        console.log('Activate', JSON.parse(JSON.stringify(data)));
    }

    onDeactivate(data): void {
        console.log('Deactivate', JSON.parse(JSON.stringify(data)));
    }

    reset() {
        this.isPatientSelected = false;
        this.activities = [];
        this.single = [];
    };

    onSubmit(customerData, patientId) {
        // Process checkout data here
        console.warn('Your order has been submitted', customerData);
        this.recomandations[patientId] = customerData;
        this.checkoutForm.reset();
    }
}


